cmake_minimum_required(VERSION 3.12)
project(os_prj)

set(CMAKE_CXX_STANDARD 14)

add_executable(os_prj main.cpp)