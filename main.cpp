using namespace std;
#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <ostream>

#define MaxSize 10000
#define Theta 0.5
#define Fault 0.2

struct Task{
    int id = -1;
    int arrivalTime;
    int executionTime;
    int relativeDeadLine;
    int realDeadLine;
    float u = 0;
    int executionCount=1;
    bool faulty = false;
    bool critical = false;
    bool feasibility = true;

    void getTask(){
        scanf("%d %d %d %d",&id, &arrivalTime, &executionTime, &relativeDeadLine);
        realDeadLine = arrivalTime + relativeDeadLine;
        u = (float) executionTime/relativeDeadLine;
        critical = u >= Theta;
    }

    void show(){
        printf("{id:%d, arrivalTime:%d, executionTime:%d, relativeDeadLine:%d,"
               " realDeadLine:%d, u:%f, executionCount:%d, faulty:%d, critical:%d feasibility:%d}\n",
               id, arrivalTime, executionTime, relativeDeadLine, realDeadLine, u,
               executionCount, faulty, critical, feasibility);
    }
};

template <class t>
struct Queue{
private:
    int front = 0;
    int rear = 0;
    int elementCount=0;
    t queue[MaxSize];

public:
    Queue(int size) {
//        queue = (t*) malloc(size * sizeof(t));
    }

    t fronty(){
        return queue[front];
    }

    void enQueue(t element){
        elementCount++;
        queue[rear++] = element;
    }

    t deQueue(){
        elementCount--;
        t element = queue[front++];
        return element;
    }

    int size(){
        return elementCount;
    }

    bool isEmpty(){
        return size()==0;
    }

    void sort(int(*compare)(const void*, const void*)){
        qsort(queue, elementCount, sizeof(Task), compare);
    }

    void show(){
        for (int i = front; i < rear; ++i) {
            queue[i].show();
        }
    }
};

struct Core{
    int taskId = -1;
    int remainTime;

    void show(){
        printf("taskId:%d\tremainTime:%d\n", taskId, remainTime);
    }
};

struct Cores{
private:
    int size;

public:
    Core list[MaxSize];

    Cores(int tSize){
        size=tSize;
//        list = (Core*) malloc(size * sizeof(Core));
        for (int i = 0; i < size; ++i) {
            list[i].remainTime=0;
        }
    }

    int findMin(){
        int min = 1<<30;
        for (int i = 0; i < size; ++i) {
            if(list[i].remainTime<min) min=list[i].remainTime;
        }
        return min;
    }

    int setCore(){
        int min = findMin();
        for (int i = 0; i < size; ++i) {
            list[i].remainTime -= min;
        }
        return min;
    }

    void setCore(int d){
        for (int i = 0; i < size; ++i) {
            if(list[i].remainTime>d){
                list[i].remainTime -= d;
            } else{
                list[i].remainTime = 0;
            }
        }
    }

    int freeCore(){
        for (int i = 0; i < size; ++i) {
            if(list[i].remainTime == 0)return i;
        }
        return -1;
    }

    void show(){
        printf("Cores:\n");
        for (int i = 0; i < size; ++i) {
            printf("Core%d: ",i);
            list[i].show();
        }
    }
};

int compare(const void * a, const void * b) {
    Task* aTask = (Task*)a;
    Task* bTask = (Task*)b;
    return static_cast<int>((*aTask).realDeadLine - (*bTask).realDeadLine);
}

int main() {
    int numberOfCore, numberOfTask;

    printf("enter number of core and number of task: c t\n");
    scanf("%d %d", &numberOfCore, &numberOfTask);
    Cores cores(numberOfCore);
    Queue<Task> readyList(4*numberOfTask + 1);

    for (int i = 0; i < numberOfTask; ++i) {
          Task task;
          task.getTask();
          readyList.enQueue(task);
    }

    readyList.sort(compare);

    int freeCore = -1;
    int time = 0;
    bool feasibility = true;
    int faultyCore = -1;

    readyList.show();

    while(!readyList.isEmpty()){

        printf("###################### Before\n");
        printf("time:%d\tFaulty Core:%d\tFree Core:%d\n", time, faultyCore, freeCore);
        cores.show();


        time += cores.setCore();

        freeCore = cores.freeCore();

        if(freeCore == -1)continue;

        Task task = readyList.fronty();
        if(task.arrivalTime > time){
            int gap = task.arrivalTime - time;
            cores.setCore(gap);
            time += gap;
            continue;
        }

        task = readyList.deQueue();

        if(task.executionCount == 1){
            faultyCore = rand()%numberOfCore;
            int faultTime = 0;


            if(freeCore == faultyCore){     //check for faulty core
                faultTime = static_cast<int>(task.executionTime * Fault);
                task.faulty = true;
            }

            if(time + task.executionTime > task.realDeadLine){
                feasibility = false;
                task.feasibility = false;
                printf("not feasible task:");
                task.show();
            } else{
                if (!task.critical){
                    if(time + task.executionTime + faultTime > task.realDeadLine){
                        feasibility = false;
                        task.feasibility = false;
                        printf("not feasible task:");
                        task.show();
                    }
                    cores.list[freeCore].remainTime = task.executionTime + faultTime;
                    cores.list[freeCore].taskId = task.id;

                } else{
                    if (time + task.executionTime + faultTime > task.realDeadLine){
                        task.feasibility = false;
                        printf("not feasible task:");
                        task.show();
                    }
                    cores.list[freeCore].remainTime = task.executionTime + faultTime;
                    cores.list[freeCore].taskId = task.id;

                    task.executionCount++;

                    freeCore = cores.freeCore();
                    if(freeCore != -1){       //copy is fault free
                        cores.list[freeCore].remainTime = task.executionTime;
                        cores.list[freeCore].taskId = task.id;
                    } else{             //put replica copy in readyList
                        readyList.enQueue(task);
                    }
                }
            }

        } else if(task.executionCount>1 && task.faulty){
            if(time + task.executionTime > task.realDeadLine && !task.feasibility) {  //both copy and original is not feasible
                feasibility = false;
                printf("not feasible task:");
                task.show();
            } else{
                cores.list[freeCore].remainTime = task.executionTime;
                cores.list[freeCore].taskId = task.id;
            }

        }
        printf("###################### After\n");
        printf("time:%d\tFaulty Core:%d\tFree Core:%d\tcurrent Task:%d\n", time, faultyCore, freeCore, task.id);
        cores.show();
    }

    time += cores.setCore();

    printf("######################\ntime: %d\n", time);
    cores.show();

    printf("Feasibility: ");
    printf(feasibility ? "true" : "false");

    return 0;
}